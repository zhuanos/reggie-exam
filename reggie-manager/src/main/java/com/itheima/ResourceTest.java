package com.itheima;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class ResourceTest {
    public static void main(String[] args) throws IOException {
        Resource resource = new ClassPathResource("mybatis/mybatis-config.xml");
        InputStream inputStream = resource.getInputStream();
        char[] buffer = new char[1024];    //根据需要的数组⼤⼩进⾏⾃定义
        StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, "UTF-8");
        for (int numRead; (numRead = in.read(buffer, 0, buffer.length)) > 0; ) {
            out.append(buffer, 0, numRead);
        }
        String myString =  out.toString();
        System.out.println("myString = " + myString);
    }
}
