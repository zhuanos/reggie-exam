package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @Author: zhuan
 * @Desc: 瑞吉点餐-后端管理系统
 * @Date: 2022-06-04 22:05:22
 */
@SpringBootApplication
public class ReggieManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReggieManagerApplication.class, args);
    }

}
