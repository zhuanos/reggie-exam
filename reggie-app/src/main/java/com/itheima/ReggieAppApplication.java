package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @Author: zhuan
 * @Desc: 瑞吉点餐-客户端APP
 * @Date: 2022-06-04 22:01:30
 */
@SpringBootApplication
public class ReggieAppApplication {

    public static void main(String[] args) {
        System.out.println("你吃了吗");
        SpringApplication.run(ReggieAppApplication.class, args);
    }

}
